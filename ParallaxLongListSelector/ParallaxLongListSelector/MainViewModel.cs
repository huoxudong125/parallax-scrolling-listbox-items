﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ParallaxLongListSelector
{
    public class MainViewModel
    {
        public ObservableCollection<ImageSource> Images { get; private set; }
        
        public MainViewModel()
        {
            Images = new ObservableCollection<ImageSource>();
            
            for (int i = 0; i < 10; i++)
            {
                var bmp = new BitmapImage(new Uri("/Assets/TestImage.jpg", UriKind.Relative));

                Images.Add(bmp);
            }
        }
    }
}
