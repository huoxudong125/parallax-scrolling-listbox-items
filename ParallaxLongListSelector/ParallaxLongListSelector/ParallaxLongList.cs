﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using Microsoft.Phone.Controls;

namespace ParallaxLongListSelector
{
    public class ParallaxLongList : LongListSelector
    {
        private ViewportControl _viewPort;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            _viewPort = (ViewportControl)GetTemplateChild("ViewportControl");
            _viewPort.ViewportChanged += OnViewportChanged;
        }

        private void OnViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            ScrollPosition = _viewPort.Viewport.Top;
        }

        public double ScrollPosition
        {
            get { return (double)GetValue(ViewPortProperty); }
            set { SetValue(ViewPortProperty, value); }
        }

        public static readonly DependencyProperty ViewPortProperty = DependencyProperty.Register(
            "ScrollPosition",
            typeof(double),
            typeof(ParallaxLongList),
            new PropertyMetadata(0d));
    }
}
